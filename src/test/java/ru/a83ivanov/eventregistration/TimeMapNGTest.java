/*
 * Events registrator
 *
 * @author  Ivanov Andrey
 * @version 1.0
 */

package ru.a83ivanov.eventregistration;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Тестирование механизма учета событий в рамках различных временных периодов
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public class TimeMapNGTest extends Assert {

    private final static long MILLISECONDS_IN_SECONDS = TimeUnit.SECONDS.toMillis(1);
    
    @DataProvider
    public Object[][] getDataForTestPeriodMapping() {

        return new Object[][] {
            {
                3,
                1,
                "2016-01-01 12:00:03",
                new String[] {
                    "2016-01-01 12:00:00",
                    "2016-01-01 12:00:01",
                    "2016-01-01 12:00:02",
                },
                3
            },            
            {
                3,
                1,
                "2016-01-01 12:00:03",
                new String[] {
                    "2016-01-01 12:00:00",
                    "2016-01-01 12:00:01",
                    "2016-01-01 12:00:02",
                    "2016-01-01 12:00:04",
                },
                2
            },
            {
                3,
                1,
                "2016-01-01 12:00:03",
                new String[] {},
                0
            },
            {
                3,
                1,
                "2016-01-01 12:00:33",
                new String[] {
                    "2016-01-01 12:00:00",
                    "2016-01-01 12:00:01",
                    "2016-01-01 12:00:02",
                    "2016-01-01 12:00:04",
                },
                0
            },
            {
                3,
                1,
                "2016-01-01 12:00:03",
                new String[] {
                    "2016-01-01 12:00:00",
                    "2016-01-01 12:00:01",
                    "2016-01-01 12:00:01",
                    "2016-01-01 12:00:01",
                    "2016-01-01 12:00:02",
                    "2016-01-01 12:00:02",
                    "2016-01-01 12:00:02",
                    "2016-01-01 12:00:04",
                },
                2
            },
            {
                3,
                20 * 60,
                "2016-01-01 12:40:03",
                new String[] {
                    "2016-01-01 11:00:00",
                    "2016-01-01 11:15:01",
                    "2016-01-01 12:05:01",
                    "2016-01-01 12:07:01",
                    "2016-01-01 12:20:00",
                    "2016-01-01 12:40:00",
                    "2016-01-01 12:39:59",
                },
                2
            },
            {
                3,
                20 * 60,
                "2016-01-01 12:40:03",
                new String[] {
                    "2016-01-01 11:00:00",
                    "2016-01-01 11:15:01",
                    "2016-01-01 12:05:01",
                    "2016-01-01 12:07:01",
                    "2016-01-01 12:21:00",
                    "2016-01-01 12:40:00",
                    "2016-01-01 12:39:59",
                },
                2
            }, 
        };
    }
    
    @Test(
        dataProvider = "getDataForTestPeriodMapping"
    )
    public void testPeriodMapping(int timeMapSize, long periodLength,
            String currentTime, String[] items, long expectedResult)
            throws ParseException {
        
        TimeMap map = new TimeMap(timeMapSize, periodLength);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        for(String date : items) {
            map.createNewItem(df.parse(date).getTime());
        }
        
        assertEquals(map.getCount(df.parse(currentTime).getTime()), expectedResult);
    }
    
    @DataProvider
    public Object[][] getDataForTestCreateNewItem() {
        
        return new Object[][] {
            {3, 1, 5},
            {3, 60, 5},
            {3, 3600, 5},
            {3, 1, 25},
            {3, 60, 25},
            {3, 3600, 25},
            {1, 1, 5},
            {1, 60, 5},
            {1, 3600, 5},
            {5, 5, 5},
            {5, 121, 5},
            {5, 3555, 5},
        };
    }
    
    @Test(
        dataProvider = "getDataForTestCreateNewItem"
    )
    public void testCreateNewItem(int timeMapSize, long periodLength,
            int repeatsToCheck)
            throws NoSuchFieldException,
                   IllegalArgumentException,
                   IllegalAccessException {

        TimeMap map = new TimeMap(timeMapSize, periodLength);
        long time = System.currentTimeMillis();
        
        for(int i = 0; i < timeMapSize; i++) {
            long key = time + i * MILLISECONDS_IN_SECONDS * periodLength;
            map.createNewItem(key);
            
            AtomicLong counter = map.get(key);
            assertNotNull(counter);
        }
            
        Field hashMapField = map.getClass().getDeclaredField("map");
        hashMapField.setAccessible(true);
        HashMap hashMap = (HashMap) hashMapField.get(map);
        
        assertEquals(hashMap.size(), timeMapSize);
        
        for(int i = 0; i < repeatsToCheck; i++) {
            map.createNewItem(
                time + (timeMapSize + i) * MILLISECONDS_IN_SECONDS * periodLength
            );
            assertEquals(hashMap.size(), timeMapSize);
        }
    }    
    
}

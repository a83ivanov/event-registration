/*
 * Events registrator
 *
 * @author  Ivanov Andrey
 * @version 1.0
 */

package ru.a83ivanov.eventregistration;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Тестирование класса объектов для регистрации событий
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public class EventRegistratorImplNGTest extends Assert {

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    @DataProvider
    public Object[][] getEventsData() throws ParseException {
        
        return new Object[][] {
            {
                generateEventList("2016-01-01 00:00:00", 60, 1),
                10,
                60,
                60,
                60,
                "2016-01-01 00:00:01"
            },
            {
                generateEventList("2016-01-01 00:00:00", 500, 10),
                50,
                188,
                500,
                500,
                "2016-01-01 00:00:01"
            },
            {
                generateEventList("2016-01-01 00:00:00", 5000, 10),
                500,
                0,
                0,
                5000,
                "2016-01-01 00:12:00"
            },             
        };
    }
        
    @Test(
        dataProvider = "getEventsData"
    )
    public void checkRegister(final List<Event> eventsList, int threadPoolSize,
            int minuteExpectedEventsCount, int hourExpectedEventsCount,
            int dayExpectedEventsCount, String currentTime)
            throws ParseException,
                   InterruptedException,
                   ExecutionException {

        final EventRegistrator registrator = new EventRegistratorImpl(2, 1, 1);
        
        ExecutorService pool = Executors.newFixedThreadPool(threadPoolSize);
        Set<Future<Boolean>> set = new HashSet<>();
        
        for(int i = 0; i < threadPoolSize; i++) {
            set.add(
                pool.submit(new Callable() {
                    final private List<Event> events = eventsList;
                    final private EventRegistrator reg = registrator;
                    private int offset;
                    private int count;
                    @Override
                    public Object call() throws Exception {
                        int index = 0;
                        while(true) {
                            int eventIndex = index++ * count + offset;

                            if(eventIndex >= events.size()) {
                                break;
                            }

                            Event event = events.get(eventIndex);
                            if(event == null) {
                                break;
                            }
                            reg.register(event);
                        }
                        return true;
                    }
                    public Callable setOffsetAndCount(int offset, int count) {
                        this.offset = offset; this.count = count; return this;
                    }
                }.setOffsetAndCount(i, threadPoolSize))
            );
        }
        
        for(Future<Boolean> future : set){
            future.get();
        }        
        
        pool.shutdownNow();
        
        assertEquals(
            registrator.getLastMinuteCount(df.parse(currentTime).getTime()),
            minuteExpectedEventsCount
        );
        assertEquals(
            registrator.getLastHourCount(df.parse(currentTime).getTime()),
            hourExpectedEventsCount
        );
        assertEquals(
            registrator.getLastDayCount(df.parse(currentTime).getTime()),
            dayExpectedEventsCount
        );
        
    }

    private List<Event> generateEventList(String generateStartTime, int count,
            int step)
            throws ParseException {

        List<Event> eventsList = new ArrayList(count);
        
        for(int i = 0; i < count; i++) {
            eventsList.add(new Event(){
                private long time;
                Event setEventTime(long time) { this.time = time; return this; }
                @Override
                public long getEventTime() { return this.time; }
            }.setEventTime(df.parse(generateStartTime).getTime() + i * step));
        }
        
        return eventsList;
    }

}

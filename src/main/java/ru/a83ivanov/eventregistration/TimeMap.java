/*
 * Events registrator
 *
 * @author  Ivanov Andrey
 * @version 1.0
 */

package ru.a83ivanov.eventregistration;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * Инкапсулирует логику работы с временными интервалами
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
class TimeMap {
    
    /**
     * Количество миллисекунд в одной секунде
     */
    private final static long MILLISECONDS_IN_SECOND = TimeUnit.SECONDS.toMillis(1);
    
    /**
     * HashMap, на котором базируется логика работы с объектами класса
     */
    private final HashMap<Long, AtomicLong> map;

    /**
     * Период учета в секундах
     */
    private final long seconds;

    /**
     * Количество периодов учета
     */
    private final int size;

    /**
     * Время начала последнего добавленного интервала в секундах
     */
    volatile private long latestCreatedItem = 0;

    /**
     * Конструктор
     * 
     * @param size              Количество последних интервалов учета
     * @param periodInSeconds   Период интервала в секундах
     */
    TimeMap(int size, long periodInSeconds) {

        map = new HashMap(size);
        this.size = size;
        this.seconds = periodInSeconds;
    }

    /**
     * Возвращает объект счетчика для определенного временного интервала
     * 
     * @param timeInMilliseconds UNIX-время в миллисекундах
     * @return Объект счетчика или null, если не удалось его найти
     */
    public AtomicLong get(long timeInMilliseconds) {

        return map.get(convertTimeToKey(timeInMilliseconds));
    }

    /**
     * Формирует новый элемент для учета наступившего интервала
     * 
     * При этому удаляет старые интервалы, в которых уже нет необходимости
     * 
     * @param timeInMilliseconds Время в рамках нового интервала
     */
    synchronized public void createNewItem(long timeInMilliseconds) {
        
        clearOldItems();
        
        Long currentTime = convertTimeToKey(timeInMilliseconds);

        map.put(currentTime, new AtomicLong(1));
        latestCreatedItem =  currentTime;
    }
    
    /**
     * Определяет количество зарегистрированных событий, произошедших до определенного времени
     * 
     * @param timeInMilliseconds Время, относительно которого необходимо делать расчет
     * @return Количество зарегистрированных событий
     */
    public long getCount(long timeInMilliseconds) {
        
        Long currentTime = convertTimeToKey(timeInMilliseconds);
        long result = 0;
        
        for(long i = 0; i <= size; i++) {
            try {
                result += map.get(currentTime - i * seconds).longValue();
            } catch(NullPointerException e) {}
        }
        
        return result;
    }
    
    /**
     * Удаляет интервалы, для которых уже не нужно вести учет
     */
    private void clearOldItems() {

        if(latestCreatedItem != 0) {
            for(long i = -1; i < size * 2; i++) {
                map.remove(latestCreatedItem - (size + i) * seconds);
            }
        }
    }    
    
    /**
     * Метод вычисляет время начала интервала для события в секундах
     * 
     * @param timeInMilliseconds Время события
     * @return Время начала для интервала в секундах
     */
    private Long convertTimeToKey(long timeInMilliseconds) {
        
        return (timeInMilliseconds / (MILLISECONDS_IN_SECOND * seconds)) * seconds;
    }
}

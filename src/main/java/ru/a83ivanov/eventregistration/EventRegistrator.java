/*
 * Events registrator
 *
 * @author  Ivanov Andrey
 * @version 1.0
 */

package ru.a83ivanov.eventregistration;

/**
 * Интерфейс регистратора
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public interface EventRegistrator {
    
    /**
     * Регистрирует событие
     * 
     * @param event Объект события
     */
    void register(Event event);
    
    /**
     * Возвращает количество событий за последнюю минуту
     * 
     * @param currentTimeInMilliseconds Текущее время, от которого отсчитываем
     * @return Количество событий
     */
    long getLastMinuteCount(long currentTimeInMilliseconds);
    
    /**
     * Возвращает количество событий за последний час
     * 
     * @param currentTimeInMilliseconds Текущее время, от которого отсчитываем
     * @return Количество событий
     */
    long getLastHourCount(long currentTimeInMilliseconds);
    
    /**
     * Возвращает количество событий за последние 24 часа
     * 
     * @param currentTimeInMilliseconds Текущее время, от которого отсчитываем
     * @return Количество событий
     */
    long getLastDayCount(long currentTimeInMilliseconds);
}

/*
 * Events registrator
 *
 * @author  Ivanov Andrey
 * @version 1.0
 */

package ru.a83ivanov.eventregistration;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Класс объектов для регистрации событий
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public class EventRegistratorImpl implements EventRegistrator {

    /**
     * Объект для регистрации событий посекундно
     */
    private final TimeMap secondsMap;
    
    /**
     * Объект для регистрации событий поминутно
     */
    private final TimeMap minutesMap;
    
    /**
     * Объект для регистрации событий в рамках последних часов
     */
    private final TimeMap hoursMap;
    
    /**
     * Коллекция объектов для регистрации событий
     */
    private final List<TimeMap> timeMaps;
    
    /**
     * Конструктор
     * 
     * @param secondsMapSize Количество периодов учета для секунд
     * @param minutesMapSize Количество периодов учета для минут
     * @param hoursMapSize   Количество периодов учета для часов
     */
    public EventRegistratorImpl(int secondsMapSize, int minutesMapSize,
            int hoursMapSize) {
        
        secondsMap = new TimeMap(secondsMapSize, 1);
        minutesMap = new TimeMap(minutesMapSize, TimeUnit.MINUTES.toSeconds(1));
        hoursMap   = new TimeMap(hoursMapSize, TimeUnit.HOURS.toSeconds(1));
        
        timeMaps = new ArrayList();
        timeMaps.add(secondsMap);
        timeMaps.add(minutesMap);
        timeMaps.add(hoursMap);        
    }
    
    @Override
    public void register(Event event) {
        
        long time = event.getEventTime();
        
        // Перебераем все Map объекты
        timeMaps.stream().forEach((map) -> {
            
            // Получаем ссылку на счетчик событий для текущего Map объекта
            AtomicLong counter = map.get(time);
            
            if(counter == null) {
                // Если не удалось получить ссылку на элемент, входим в режим
                // синхронизации в рамках Map объекта
                synchronized(map) {
                    // Проверяем не создал ли уже какой-то другой поток требуемый
                    // элемент
                    counter = map.get(time);

                    if(counter == null) {
                        // Если элемент не был создан, значит текущий поток
                        // должен это сделать
                        // Счетчик при создании получает значение 1, поэтому
                        // менять его при создании не требуется
                        map.createNewItem(time);
                    }
                    else
                    {
                        // Если элемент уже существует, обновляем счетчик
                        counter.incrementAndGet();
                    }
                }
            } else {
                // Возможно другой поток за это время успел удалить элемент,
                // поэтому требуется быть готовыми к исключению
                try {
                    counter.incrementAndGet();
                } catch (NullPointerException e) {
                    // Если было выброшено исключение, ничего не делаем, так как
                    // это обозначает, что время соответствующего периода уже
                    // закончилось
                }
            }
        });
        
        // Какие-то доп. действия по обработке события
        registerEvent(event);
    }

    @Override
    public long getLastMinuteCount(long currentTimeInMilliseconds) {
        
        return secondsMap.getCount(currentTimeInMilliseconds);
    }

    @Override
    public long getLastHourCount(long currentTimeInMilliseconds) {
        
        return minutesMap.getCount(currentTimeInMilliseconds);
    }

    @Override
    public long getLastDayCount(long currentTimeInMilliseconds) {
        
        return hoursMap.getCount(currentTimeInMilliseconds);
    }
    
    /**
     * Метод для выполнения допольнительных действий с объектом события
     * 
     * @param event Объект события
     */
    protected void registerEvent(Event event) {
        
    }

}

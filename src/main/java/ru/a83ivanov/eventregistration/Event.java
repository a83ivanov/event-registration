/*
 * Events registrator
 *
 * @author  Ivanov Andrey
 * @version 1.0
 */

package ru.a83ivanov.eventregistration;

/**
 * Интерфейс для объектов-событий
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public interface Event {
    
    /**
     * Возвращает время события в миллисекундах
     * 
     * @return Время события
     */
    long getEventTime();
    
}
